package com.chrisgammage.gtwit.client;

import com.google.gwt.core.client.JavaScriptObject;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/30/12
 * Time: 10:57 PM
 */
public class TweetUrl implements Serializable {

    private String expanded_url;
    private String url;
    private Integer[] indices;
    private String display_url;

    public String getExpanded_url() {
        return expanded_url;
    }

    public void setExpanded_url(String expanded_url) {
        this.expanded_url = expanded_url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer[] getIndices() {
        return indices;
    }

    public void setIndices(Integer[] indices) {
        this.indices = indices;
    }

    public String getDisplay_url() {
        return display_url;
    }

    public void setDisplay_url(String display_url) {
        this.display_url = display_url;
    }
}
