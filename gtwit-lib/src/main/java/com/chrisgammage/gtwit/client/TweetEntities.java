package com.chrisgammage.gtwit.client;

import com.google.gwt.core.client.JavaScriptObject;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/30/12
 * Time: 10:36 PM
 */
public class TweetEntities implements Serializable {

    private TweetUrl[] urls;
    private TweetHashTag[] hashtags;
    private TweetUserMention[] user_mentions;
    private TweetMedia[] media;

    public TweetUrl[] getUrls() {
        return urls;
    }

    public void setUrls(TweetUrl[] urls) {
        this.urls = urls;
    }

    public TweetHashTag[] getHashtags() {
        return hashtags;
    }

    public void setHashtags(TweetHashTag[] hashtags) {
        this.hashtags = hashtags;
    }

    public TweetUserMention[] getUser_mentions() {
        return user_mentions;
    }

    public void setUser_mentions(TweetUserMention[] user_mentions) {
        this.user_mentions = user_mentions;
    }

    public TweetMedia[] getMedia() {
        return media;
    }

    public void setMedia(TweetMedia[] media) {
        this.media = media;
    }
}
