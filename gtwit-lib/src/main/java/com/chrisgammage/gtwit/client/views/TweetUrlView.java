package com.chrisgammage.gtwit.client.views;

import com.chrisgammage.gtwit.client.TweetUrl;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/2/12
 * Time: 12:21 AM
 */
public class TweetUrlView extends Composite {

    interface TweetUrlViewUiBinder extends UiBinder<Label, TweetUrlView> {
    }

    private static TweetUrlViewUiBinder ourUiBinder = GWT.create(TweetUrlViewUiBinder.class);
    @UiField
    Label label;

    private TweetUrl tweetUrl;

    public TweetUrlView(TweetUrl tweetUrl) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.tweetUrl = tweetUrl;
        label.setText(tweetUrl.getDisplay_url());
    }

    @UiHandler("label")
    public void onHover(MouseOverEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("label")
    public void onHover(MouseOutEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }

    @UiHandler("label")
    public void onClickUp(MouseUpEvent event) {
        Window.open(tweetUrl.getExpanded_url(), null, null);
    }
}