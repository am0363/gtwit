package com.chrisgammage.gtwit.client.views;

import com.chrisgammage.gtwit.client.TweetMedia;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;


/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/2/12
 * Time: 10:58 PM
 */
public class TweetMediaView extends Composite {
    interface TweetMediaViewUiBinder extends UiBinder<Label, TweetMediaView> {
    }

    private static TweetMediaViewUiBinder ourUiBinder = GWT.create(TweetMediaViewUiBinder.class);
    @UiField
    Label label;

    public TweetMediaView(TweetMedia media) {
        initWidget(ourUiBinder.createAndBindUi(this));
        label.setText(media.getDisplay_url());
    }

    @UiHandler("label")
    public void onHover(MouseOverEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("label")
    public void onHover(MouseOutEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }

    @UiHandler("label")
    public void onClickUp(MouseUpEvent event) {
        /*Window.open("https://twitter.com/intent/user?screen_name=" + userName,
                displayName + " (@" + userName + ") on Twitter", "enabled,width=550,height=520");*/
    }
}