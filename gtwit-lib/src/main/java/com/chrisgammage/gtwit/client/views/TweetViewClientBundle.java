package com.chrisgammage.gtwit.client.views;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/1/12
 * Time: 11:12 PM
 */
public interface TweetViewClientBundle extends ClientBundle {
    interface MyCssResource extends CssResource {
        String tweetViewInner();
        String tweetViewText();
        String tweetViewBody();
        String tweetUserText();
        String tweetHashTagText();
        String tweetUrlText();
        String tweetUserMentionText();
        String tweetMediaText();
    }

    @Source("TweetView.css")
    MyCssResource css();
}
