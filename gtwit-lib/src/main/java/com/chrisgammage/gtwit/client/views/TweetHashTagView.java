package com.chrisgammage.gtwit.client.views;

import com.chrisgammage.gtwit.client.TweetHashTag;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/2/12
 * Time: 12:11 AM
 */
public class TweetHashTagView extends Composite {
    interface TweetHashTagViewUiBinder extends UiBinder<Label, TweetHashTagView> {
    }

    private static TweetHashTagViewUiBinder ourUiBinder = GWT.create(TweetHashTagViewUiBinder.class);

    private TweetHashTag tweetHashTag;
    @UiField
    Label label;

    public TweetHashTagView(TweetHashTag tweetHashTag) {
        this.tweetHashTag = tweetHashTag;
        initWidget(ourUiBinder.createAndBindUi(this));
        label.setText("#" + tweetHashTag.getText());
    }

    @UiHandler("label")
    public void onHover(MouseOverEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("label")
    public void onHover(MouseOutEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }

    @UiHandler("label")
    public void onClickUp(MouseUpEvent event) {
        Window.open("https://twitter.com/#!/search?q=%23" + tweetHashTag.getText(), null, null);
    }
}