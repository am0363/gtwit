package com.chrisgammage.gtwit.client;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/2/12
 * Time: 10:44 PM
 */
public class TweetMediaSizes implements Serializable {

    private TweetMediaSize large;
    private TweetMediaSize medium;
    private TweetMediaSize small;
    private TweetMediaSize thumb;

    public TweetMediaSize getLarge() {
        return large;
    }

    public void setLarge(TweetMediaSize large) {
        this.large = large;
    }

    public TweetMediaSize getMedium() {
        return medium;
    }

    public void setMedium(TweetMediaSize medium) {
        this.medium = medium;
    }

    public TweetMediaSize getSmall() {
        return small;
    }

    public void setSmall(TweetMediaSize small) {
        this.small = small;
    }

    public TweetMediaSize getThumb() {
        return thumb;
    }

    public void setThumb(TweetMediaSize thumb) {
        this.thumb = thumb;
    }
}
