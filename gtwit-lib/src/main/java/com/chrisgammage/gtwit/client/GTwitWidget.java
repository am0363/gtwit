package com.chrisgammage.gtwit.client;

import com.chrisgammage.gtwit.client.views.TweetView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.VerticalPanel;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/30/12
 * Time: 10:06 PM
 */
public class GTwitWidget extends ResizeComposite {

    private String userId;
    private int count;
    private Tweet[] tweets;

    private static Logger LOG = Logger.getLogger(GTwitWidget.class.getName());

    interface GTwitWidgetUiBinder extends UiBinder<LayoutPanel, GTwitWidget> {
    }

    private static GTwitWidgetUiBinder ourUiBinder = GWT.create(GTwitWidgetUiBinder.class);
    @UiField
    VerticalPanel verticalPanel;

    @UiField
    Image profileImage;
    @UiField
    Label displayName;
    @UiField
    Label userName;
    @UiField
    Image footerImage;
    @UiField
    Label footerText;

    public GTwitWidget() {
        initWidget(ourUiBinder.createAndBindUi(this));
        if (userId != null) {
            userName.setText(userId);
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        userName.setText(userId);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private void renderResults() {
        if (tweets != null && tweets.length > 0) {
            if (tweets[0].getUser().getProfile_image_url() != null) {
                profileImage.setUrl(tweets[0].getUser().getProfile_image_url());
            }
            if (tweets[0].getUser().getName() != null) {
                displayName.setText(tweets[0].getUser().getName());
            }
            for (Tweet tweet : tweets) {
                verticalPanel.add(new TweetView(tweet));
            }
        }
    }

    public void fetchTweets() {
        GTwitService.App.getInstance().getTweets(userId, count, new AsyncCallback<Tweet[]>() {
            public void onFailure(Throwable caught) {
                LOG.log(Level.SEVERE, "Failed to get Tweets", caught);
            }

            public void onSuccess(Tweet[] result) {
                LOG.log(Level.INFO, "Got Tweets");
                tweets = result;

                renderResults();
            }
        });
    }

    @UiHandler("footerImage")
    void onImageMouseUp(MouseUpEvent event) {
        Window.open("https://twitter.com", null, null);
    }

    @UiHandler("footerText")
    void onTextMouseUp(MouseUpEvent event) {
        Window.open("https://twitter.com/" + userId, null, null);
    }

    @UiHandler("footerText")
    void onMouseOverText(MouseOverEvent event) {
        footerText.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("footerText")
    void onMouseOutText(MouseOutEvent event) {
        footerText.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }
}