package com.chrisgammage.gtwit.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/31/12
 * Time: 12:22 AM
 */
public interface GTwitServiceAsync {
    void getTweets(String userId, int count, AsyncCallback<Tweet[]> async);
}
