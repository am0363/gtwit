package com.chrisgammage.gtwit.client.views;

import com.chrisgammage.gtwit.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/31/12
 * Time: 1:12 AM
 */
public class TweetView extends Composite {

    interface TweetViewUiBinder extends UiBinder<SimplePanel, TweetView> {
    }

    private static TweetViewUiBinder ourUiBinder = GWT.create(TweetViewUiBinder.class);
    private Tweet tweet;
    private TweetViewClientBundle clientBundle = GWT.create(TweetViewClientBundle.class);


    @UiField
    SimplePanel inner;

    public TweetView(Tweet tweet) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.tweet = tweet;

        FlowPanel html = new FlowPanel();
        html.getElement().getStyle().setColor("white");

        Map<Integer, Object> startIndices = new HashMap<Integer, Object>();
        if (tweet.getEntities().getHashtags() != null) {
            for (TweetHashTag hashTag : tweet.getEntities().getHashtags()) {
                startIndices.put(hashTag.getIndices()[0], hashTag);
            }
        }
        if (tweet.getEntities().getUrls() != null) {
            for (TweetUrl url : tweet.getEntities().getUrls()) {
                startIndices.put(url.getIndices()[0], url);
            }
        }
        if (tweet.getEntities().getUser_mentions() != null) {
            for (TweetUserMention mention : tweet.getEntities().getUser_mentions()) {
                startIndices.put(mention.getIndices()[0], mention);
            }
        }
        if (tweet.getEntities().getMedia() != null) {
            for (TweetMedia media : tweet.getEntities().getMedia()) {
                startIndices.put(media.getIndices()[0], media);
            }
        }
        boolean isRetweet = tweet.getText().startsWith("RT ");
        if (!isRetweet) {
            html.add(new TweetUserNameView(tweet.getUser().getScreen_name(), tweet.getUser().getName()));
        }
        clientBundle.css().ensureInjected();
        int at = 0;
        int last = 0;
        int end = tweet.getText().length() - 1;
        boolean first = true;
        if (isRetweet) {
            at = 2;
            last = 2;
        }
        while (at < end) {
            while (startIndices.get(at) == null && at < end) {
                at++;
            }
            if (last != at) {
                String str = tweet.getText().substring(last, at);
                last = at;
                Label text = new Label();
                text.addStyleName(clientBundle.css().tweetViewText());
                text.setText(str.replaceAll("\\&amp\\;", "&"));
                html.add(text);
            }
            Object obj = startIndices.get(at);
            if (obj != null) {
                if (obj instanceof TweetHashTag) {
                    TweetHashTag tht = (TweetHashTag) obj;
                    at = tht.getIndices()[1];
                    html.add(new TweetHashTagView(tht));
                } else if (obj instanceof TweetUrl) {
                    TweetUrl tu = (TweetUrl) obj;
                    at = tu.getIndices()[1];
                    html.add(new TweetUrlView(tu));
                } else if (obj instanceof TweetUserMention) {
                    TweetUserMention tum = (TweetUserMention) obj;
                    at = tum.getIndices()[1];
                    if (isRetweet && first && at != end) {
                        at++;
                    }
                    html.add(new TweetUserMentionView(tum, !(isRetweet && first)));
                } else if (obj instanceof TweetMedia) {
                    TweetMedia m = (TweetMedia) obj;
                    at = m.getIndices()[1];
                    html.add(new TweetMediaView(m));
                }
                last = at;
                first = false;
            }
        }
        inner.add(html);
    }
}