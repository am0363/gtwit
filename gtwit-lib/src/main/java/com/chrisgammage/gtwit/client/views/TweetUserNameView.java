package com.chrisgammage.gtwit.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/1/12
 * Time: 12:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class TweetUserNameView extends Composite {
    interface TweetUserNameViewUiBinder extends UiBinder<Label, TweetUserNameView> {
    }

    private static TweetUserNameViewUiBinder ourUiBinder = GWT.create(TweetUserNameViewUiBinder.class);

    private String userName;
    private String displayName;
    @UiField
    Label label;

    public TweetUserNameView(String userName, String displayName) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.userName = userName;
        label.setText(userName);
    }

    @UiHandler("label")
    public void onHover(MouseOverEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("label")
    public void onHover(MouseOutEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }

    @UiHandler("label")
    public void onClickUp(MouseUpEvent event) {
        Window.open("https://twitter.com/intent/user?screen_name=" + userName,
                displayName + " (@" + userName + ") on Twitter", "enabled,width=550,height=520");
    }
}