package com.chrisgammage.gtwit.client.views;

import com.chrisgammage.gtwit.client.TweetUserMention;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/2/12
 * Time: 12:25 AM
 */
public class TweetUserMentionView extends Composite {

    interface TweetUserMentionViewUiBinder extends UiBinder<Label, TweetUserMentionView> {
    }

    private static TweetUserMentionViewUiBinder ourUiBinder = GWT.create(TweetUserMentionViewUiBinder.class);
    @UiField
    Label label;

    private TweetUserMention userMention;

    public TweetUserMentionView(TweetUserMention tweetUserMention, boolean showAt) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.userMention = tweetUserMention;
        if(showAt) {
            label.setText("@" + tweetUserMention.getScreen_name());
        } else {
            label.setText(tweetUserMention.getScreen_name());
        }
    }

    @UiHandler("label")
    public void onHover(MouseOverEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

    @UiHandler("label")
    public void onHover(MouseOutEvent event) {
        label.getElement().getStyle().setTextDecoration(Style.TextDecoration.NONE);
    }

    @UiHandler("label")
    public void onClickUp(MouseUpEvent event) {
        Window.open("https://twitter.com/#!/" + userMention.getScreen_name(), null, null);
    }
}