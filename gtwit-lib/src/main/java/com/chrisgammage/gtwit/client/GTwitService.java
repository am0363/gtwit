package com.chrisgammage.gtwit.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/31/12
 * Time: 12:22 AM
 */
@RemoteServiceRelativePath("GTwitService")
public interface GTwitService extends RemoteService {

    Tweet[] getTweets(String userId, int count);

    /**
     * Utility/Convenience class.
     * Use GTwitService.App.getInstance() to access static instance of GTwitServiceAsync
     */
    public static class App {
        private static final GTwitServiceAsync ourInstance = (GTwitServiceAsync) GWT.create(GTwitService.class);

        public static GTwitServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
