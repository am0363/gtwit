package com.chrisgammage.gtwit.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/30/12
 * Time: 10:14 PM
 */
public class GTwitTestView extends ResizeComposite {

    interface GTwitTestViewUiBinder extends UiBinder<LayoutPanel, GTwitTestView> {
    }

    private static GTwitTestViewUiBinder ourUiBinder = GWT.create(GTwitTestViewUiBinder.class);

    @UiField
    GTwitWidget gTwitWidget;

    public GTwitTestView() {
        initWidget(ourUiBinder.createAndBindUi(this));
        gTwitWidget.fetchTweets();
    }
}