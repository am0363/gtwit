package com.chrisgammage.gtwit.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 5/30/12
 * Time: 10:03 PM
 */
public class GTwitTest implements EntryPoint {
    public void onModuleLoad() {
        RootLayoutPanel.get().add(new GTwitTestView());
    }
}
